//
//  ContentView.swift
//  FoodDeliveryApp
//
//  Created by rafiul hasan on 25/12/21.
//

import SwiftUI

@available(iOS 15.0, *)
struct ContentView: View {
    @EnvironmentObject var viewModel: GoogleSignInViewModel
    
    var body: some View {
        if viewModel.isUserLoggedIn {
            MainTabView()
        } else {
            StartUIView()
        }
    }
}

@available(iOS 15.0, *)
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
