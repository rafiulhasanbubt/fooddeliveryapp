//
//  FoodDeliveryApp.swift
//  FoodDeliveryApp
//
//  Created by rafiul hasan on 25/12/21.
//

import UIKit
import SwiftUI
import Firebase
import CoreData
import GoogleSignIn

@available(iOS 15.0, *)
@main
struct FoodDeliveryApp: App {
    @UIApplicationDelegateAdaptor var delegate: FSAppDelegate
    let persistenceController = PersistenceController.shared
    //let context = PersistentCloudKitContainer.persistentContainer.viewContext
    
    var body: some Scene {
        WindowGroup {
            //StartUIView().environment(\.managedObjectContext, persistenceController.container.viewContext)
            ContentView()
                .environmentObject(GoogleSignInViewModel())
        }
    }
}

class FSAppDelegate: NSObject, UIApplicationDelegate {
    func application( _ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
        FirebaseApp.configure()
        print("connected to firebase")
        return true
    }
    
    //MARK: Google signin page
    func application( _ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance.handle(url)
    }
}

extension GIDConfiguration {
    /// Get GIDConfiguration instance by providing clientID from GoogleService-Info.plist file
    /// - Returns: GIDConfiguration instance
    static func getGIDConfigurationInstance() -> GIDConfiguration {
        GIDConfiguration(clientID: FirebaseApp.app()?.options.clientID ?? "")
    }
}

extension UIApplication {
    /// Get root UIViewController of application. If for whatever reason, UIViewController cannot be accessed,
    /// invoke fatalError() since UIViewController instance is crucial for application to work properly.
    /// - Returns: root UIViewController
    static func getRootViewController() throws -> UIViewController {
        let scenes = UIApplication.shared.connectedScenes
        let windowScene = scenes.first as? UIWindowScene
        let window = windowScene?.windows.first
        
        guard let uiViewController = window?.rootViewController else { fatalError() }
        return uiViewController
    }
}
