//
//  HomeView.swift
//  FoodDeliveryApp
//
//  Created by rafiul hasan on 27/12/21.
//

import SwiftUI
import GoogleSignIn
import SDWebImageSwiftUI

@available(iOS 15.0, *)
struct HomeView: View {
    @EnvironmentObject var viewModel: GoogleSignInViewModel
    private let user = GIDSignIn.sharedInstance.currentUser
    
    var body: some View {
        VStack {
            UserDetailView(user: user)
            
            Spacer()
            
            Button(action: {
                withAnimation {
                    viewModel.signOut()
                }
            }) {
                HStack {
                    Text("Sign out")
                        .fontWeight(.bold)
                }
            }
            .foregroundColor(.white)
            .padding()
            .frame(maxWidth: .infinity)
            .background(.black)
            .cornerRadius(12)
            .padding()
        }
        .padding()
    }
}

@available(iOS 15.0, *)
struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
