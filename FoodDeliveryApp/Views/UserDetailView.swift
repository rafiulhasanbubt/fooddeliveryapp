//
//  UserDetailView.swift
//  FoodDeliveryApp
//
//  Created by rafiul hasan on 27/12/21.
//

import SwiftUI
import GoogleSignIn
import SDWebImageSwiftUI

@available(iOS 15.0, *)
struct UserDetailView: View {
    let user: GIDGoogleUser?
    
    var body: some View {
        VStack {
            HStack {
                Text(user?.profile?.name ?? "")
                    .bold()
                    .font(.title2)
                
                Spacer()
                
                WebImage(url: user?.profile?.imageURL(withDimension: 75))
                    .resizable()
                    .placeholder(
                        Image(systemName: "person.fill")
                    )
                    .indicator(.activity)
                    .transition(.fade(duration: 0.5))
                    .frame(width: 75, height: 75, alignment: .center)
                    .scaledToFit()
                    .clipShape(Circle())
            }
            HStack {
                Text(user?.profile?.email ?? "")
                    .bold()
                    .font(.subheadline)
                Spacer()
            }
            
            Spacer()
        }
        .padding()
        .frame(height: 150, alignment: .center)
        .background(.thickMaterial)
        .cornerRadius(12)
    }
}

struct UserDetailView_Previews: PreviewProvider {
    static var previews: some View {
        if #available(iOS 15.0, *) {
            UserDetailView(user: GIDGoogleUser())
        } else {
            // Fallback on earlier versions
        }
    }
}
