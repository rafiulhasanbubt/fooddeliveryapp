//
//  WelcomeUIView.swift
//  FoodDeliveryApp
//
//  Created by rafiul hasan on 26/12/21.
//

import SwiftUI

@available(iOS 15.0, *)
struct WelcomeUIView: View {
    @ObservedObject var mViewModel = WelcomeViewModel()    
    @EnvironmentObject var viewModel: GoogleSignInViewModel
    
    var body: some View {
        GeometryReader { geometry in
            ScrollView {
                VStack (alignment : .center){
                    HStack(alignment: .top) {
                        Spacer()
                        NavigationLink(
                            destination: LoginUIView(),
                            isActive: self.$mViewModel.isNavigateToLoginScreen) {
                                
                                Button(action: {
                                    self.mViewModel.isNavigateToLoginScreen = true
                                }, label: {
                                    Text("Log In").foregroundColor(.pink)
                                })
                                    .padding()
                                    .border(Color.white, width: 0)
                                    .frame(width: 100, height: 50, alignment: .center)
                            }
                    }
                    
                    TabView {
                        ForEach(0..<3) { i in
                            ZStack {
                                SlideView(index: i)
                            }.clipShape(RoundedRectangle(cornerRadius: 10.0, style: .continuous))
                        }
                    }
                    .frame(width: UIScreen.main.bounds.width, height: 600, alignment: .center)
                    .accentColor(.blue)
                    .tabViewStyle(PageTabViewStyle())
                    
                    NavigationLink(
                        destination: RegisterUIView(),
                        isActive: self.$mViewModel.isNavigateToRegisterScreen) {
                            Button(action: {
                                self.mViewModel.isNavigateToRegisterScreen = true
                            }, label: {
                                Text("Create Account")
                            })
                                .frame(width: geometry.size.width - 50, height: 45, alignment: .center)
                                .accentColor(.white)
                                .background(Color.pink)
                                .addBorder(Color.pink, width: 1, cornerRadius: 20)
                                .padding(.top, 20)
                        }
                    
                    Button(action: {
                        viewModel.signIn()
                    }, label: {
                        HStack {
                            Image("google-logo")
                                .resizable()
                                .frame(width: 30, height: 30, alignment: .center)
                                .scaledToFill()
                            Text("Continue with Google")
                        }
                    })
                        .frame(width: geometry.size.width - 50, height: 45, alignment: .center)
                        .accentColor(.black)
                        .background(Color.init(UIColor.systemGray5))
                        .addBorder(Color.init(UIColor.systemGray5), width: 1, cornerRadius: 20)
                        .padding(.top, 10)
                    
                    Button(action: {
                        //Action come here
                    }, label: {
                        HStack {
                            Image("facebook_circle_64")
                                .resizable()
                                .frame(width: 30, height: 30, alignment: .center)
                                .scaledToFill()
                            Text("Continue with Facebook")
                        }
                    })
                        .frame(width: geometry.size.width - 50, height: 45, alignment: .center)
                        .accentColor(.black)
                        .background(Color.init(UIColor.systemGray5))
                        .addBorder(Color.init(UIColor.systemGray5), width: 1, cornerRadius: 20)
                        .padding(.top, 10)
                    
                    Spacer()                    
                }
                .frame(width: geometry.size.width, height: geometry.size.height, alignment: .center)
            }
            .frame(width: geometry.size.width, height: geometry.size.height, alignment: .trailing)
            .navigationBarHidden(true)
        }
    }
}

@available(iOS 15.0, *)
struct WelcomeUIView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeUIView()
    }
}
