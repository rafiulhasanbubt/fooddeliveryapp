//
//  MainTabView.swift
//  FoodDeliveryApp
//
//  Created by rafiul hasan on 26/12/21.
//

import SwiftUI

@available(iOS 15.0, *)
struct MainTabView: View {
    var body: some View {
        
        TabView {
            CartInfoView()
            .tabItem {(
                VStack {
                    Image(systemName: "house.fill")
                        .font(.system(size: 24, weight: .regular))
                    Text("home")
                }
                )}
            .tag(0)

            SearchView()
            .tabItem {(
                VStack {
                    Image(systemName: "flame.fill").font(.system(size: 24, weight: .regular))
                    Text("search")
                    }

                )}
            .tag(1)            
            
            HomeView()
                .tabItem {(
                    VStack {
                        Image(systemName: "person.fill").font(.system(size: 24, weight: .regular))
                        Text("Profile")
                    }
                    
                    )}
                .tag(2)
            
        }.accentColor(.pink)
        
    }
}

@available(iOS 15.0, *)
struct MainTabView_Previews: PreviewProvider {
    static var previews: some View {
        MainTabView()
    }
}
