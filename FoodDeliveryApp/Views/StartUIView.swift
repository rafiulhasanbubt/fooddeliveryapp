//
//  StartUIView.swift
//  FoodDeliveryApp
//
//  Created by rafiul hasan on 26/12/21.
//

import SwiftUI

@available(iOS 15.0, *)
struct StartUIView: View {
    @ObservedObject var mViewModel = StartAppViewModel()
    @State var isNavigateToWelcome : Bool = false
    
    var body: some View {
        GeometryReader { geometry in
            NavigationView {
                ScrollView {
                    VStack (alignment : .leading, spacing : -10){
                        Image("categories")
                            .resizable()
                            .background(Color.white)
                            .frame(width: geometry.size.width, height: 550, alignment: .center)
                            .scaledToFill()
                        
                        VStack (alignment : .center) {
                            Text("Happy Meals")
                                .foregroundColor(Color.white)
                                .fontWeight(.bold)
                                .font(.system(size: 40))
                                .frame(width :geometry.size.width,height: 30, alignment: .leading)
                                .padding([.leading],30)
                            
                            Text("Discover the best foods from \nover 1,000 Restaurants")
                                .foregroundColor(Color.white)
                                .fontWeight(.bold)
                                .frame(width :geometry.size.width, height: 50, alignment: .leading)
                                .multilineTextAlignment(.leading)
                                .padding([.leading,.top],30)
                            
                            NavigationLink(
                                destination: WelcomeUIView(),
                                isActive: self.$isNavigateToWelcome) {
                                    Button(action: {
                                        self.isNavigateToWelcome = true
                                    }, label: {
                                        Text("Get Started")
                                    })
                                        .frame(width: 150, height: 45, alignment: .center)
                                        .accentColor(.black)
                                        .background(Color.white)
                                        .addBorder(Color.white, width: 1, cornerRadius: 20)
                                        .padding([.leading,.top], 30)
                                    
                                }
                        }
                        .padding(30)
                        .frame(width :UIScreen.main.bounds.width, height : 250)
                        .background(Color.pink)
                        .cornerRadius(20, antialiased: true)
                    }
                    .frame(width: geometry.size.width, height: geometry.size.height, alignment: .center)
                }
                .frame(width: geometry.size.width, height: geometry.size.height, alignment: .center)
                //.edgesIgnoringSafeArea([.top, .bottom])
            }
            .navigationBarHidden(true)
        }
    }
}

@available(iOS 15.0, *)
struct StartUIView_Previews: PreviewProvider {
    static var previews: some View {
        StartUIView()
    }
}
