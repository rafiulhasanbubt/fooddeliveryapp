//
//  CustomTextField.swift
//  FoodDeliveryApp
//
//  Created by rafiul hasan on 26/12/21.
//

import SwiftUI

struct CustomTextField: View {
    @Binding var value: String
    var placeHolder: String
    var lineColor: Color
    var width: CGFloat
    
    var body: some View {
        VStack {
            TextField(self.placeHolder, text: $value)
                .padding([.leading],10)
            
            Rectangle().frame(height: self.width)
                .padding(.horizontal, 10).foregroundColor(self.lineColor)
        }
    }
}

struct CustomTextField_Previews: PreviewProvider {
    static var previews: some View {
        CustomTextField(value: .constant("custom"), placeHolder: "custom", lineColor: Color.gray, width: 2)
    }
}
