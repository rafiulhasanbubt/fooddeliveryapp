//
//  SelectionView.swift
//  FoodDeliveryApp
//
//  Created by rafiul hasan on 26/12/21.
//

import SwiftUI

struct SelectionView: View {
    @State var selectedFruit: String? = nil
    let fruit = ["apples", "pears", "bananas", "pineapples"]
    
    var body: some View {
        List {
            ForEach(fruit, id: \.self) { item in
                SelectionCell(selectedFruit: self.$selectedFruit, fruit: item)
            }
        }
    }
}

struct SelectionView_Previews: PreviewProvider {
    static var previews: some View {
        SelectionView()
    }
}
