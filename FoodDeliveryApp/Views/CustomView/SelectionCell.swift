//
//  SelectionCell.swift
//  FoodDeliveryApp
//
//  Created by rafiul hasan on 26/12/21.
//

import SwiftUI

struct SelectionCell: View {
    @Binding var selectedFruit: String?
    let fruit: String
    
    var body: some View {
        HStack {
            Text(fruit)
            Spacer()
            if fruit == selectedFruit {
                Image(systemName: "checkmark")
                    .foregroundColor(.accentColor)
            }
        }.onTapGesture {
            self.selectedFruit = self.fruit
        }
    }
}

struct SelectionCell_Previews: PreviewProvider {
    static var previews: some View {
        SelectionCell(selectedFruit: .constant("Apple"), fruit: "Apple")
    }
}
