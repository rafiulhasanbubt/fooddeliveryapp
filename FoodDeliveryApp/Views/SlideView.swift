//
//  SlideView.swift
//  FoodDeliveryApp
//
//  Created by rafiul hasan on 26/12/21.
//

import SwiftUI

struct SlideView: View {
    var index : Int
    
    var body: some View {
        VStack (alignment: .center, spacing : 10){
            
            switch(index) {
            case 0 :
                Image("welcome1")
                    .resizable()
                    .frame(width: UIScreen.main.bounds.width - 50, height: 400, alignment: .center)
                    .scaledToFill()
                
                Text("Fast Delivery").foregroundColor(.pink)
                    .fontWeight(.bold)
                    .font(.system(size: 30))
                    .multilineTextAlignment(.center)
                
                Text("Fast Deliver to your home, office and \neverywhere you are").foregroundColor(.gray)
                    .multilineTextAlignment(.center)
                
            case 1:
                Image("welcome2")
                    .resizable()
                    .frame(width: UIScreen.main.bounds.width - 50, height: 400, alignment: .center)
                    .scaledToFill()
                
                Text("Find Food You Love").foregroundColor(.pink)
                    .fontWeight(.bold)
                    .font(.system(size: 30))
                    .multilineTextAlignment(.center)
                
                Text("Discover the best foods from \nover 1000 restarurants").foregroundColor(.gray)
                    .multilineTextAlignment(.center)
                
            default :
                Image("live-tracking")
                    .resizable()
                    .frame(width: UIScreen.main.bounds.width - 50, height: 400, alignment: .center)
                    .scaledToFill()
                
                Text("Live Tracking").foregroundColor(.pink)
                    .fontWeight(.bold)
                    .font(.system(size: 30))
                    .multilineTextAlignment(.center)
                
                Text("Fast Deliver to your home, office and \neverywhere you are").foregroundColor(.gray)
                    .multilineTextAlignment(.center)
            }
        }
    }
}

struct SlideView_Previews: PreviewProvider {
    static var previews: some View {
        SlideView(index: 0)
    }
}
