//
//  ListStyleType.swift
//  FoodDeliveryApp
//
//  Created by rafiul hasan on 25/12/21.
//

import Foundation

enum ListStyleType : Int {
    case Catetory = 0
    case PopularChoice = 1
}
