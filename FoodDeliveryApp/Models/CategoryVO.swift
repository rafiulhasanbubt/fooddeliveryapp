//
//  CategoryVO.swift
//  FoodDeliveryApp
//
//  Created by rafiul hasan on 25/12/21.
//

import Foundation

class CategoryVO : Identifiable {
    var id: Int?
    var name: String?
    var image : String?
}
