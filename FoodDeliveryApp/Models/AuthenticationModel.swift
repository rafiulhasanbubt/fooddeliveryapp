//
//  AuthenticationModel.swift
//  FoodDeliveryApp
//
//  Created by rafiul hasan on 25/12/21.
//

import Foundation

protocol AuthenticationModel {
    func login(email: String, password: String, onSuccess: @escaping () -> Void, onFailure: @escaping (String) -> Void)
    func register(userInfo: UserInfoVO, onSuccess: @escaping  () -> Void, onFailure: @escaping (String) -> Void)
    func getUserInfo() -> UserInfoVO
    func updateProfile(imageData : Data, info : UserInfoVO,success : @escaping (String) -> Void, fail : @escaping (String) -> Void)
}
