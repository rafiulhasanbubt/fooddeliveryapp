//
//  UserInfoVO.swift
//  FoodDeliveryApp
//
//  Created by rafiul hasan on 25/12/21.
//

import Foundation

class UserInfoVO : Identifiable {
    var id: Int?
    var name: String?
    var email: String?
    var password : String?
    var phone : String?
    var image : String?
}
