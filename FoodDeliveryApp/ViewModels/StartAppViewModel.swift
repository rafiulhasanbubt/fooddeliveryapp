//
//  StartAppViewModel.swift
//  FoodDeliveryApp
//
//  Created by rafiul hasan on 26/12/21.
//

import Foundation
import SwiftUI
import Combine

class StartAppViewModel: ObservableObject {
    
    let mModel : DataModel = DataModelImpl()
    
    init() {
        mModel.setupRemoteConfigDefaultVlaues()
        mModel.fetchRemoteConfigs()
    }
}
