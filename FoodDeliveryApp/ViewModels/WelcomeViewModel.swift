//
//  WelcomeViewModel.swift
//  FoodDeliveryApp
//
//  Created by rafiul hasan on 26/12/21.
//

import Foundation
import SwiftUI
import Combine

class WelcomeViewModel: ObservableObject {
    @Published var mEmail : String = ""
    @Published var mPassword : String = ""
    @Published var isError : Bool = false
    @Published var errorMessage: String = ""
    @Published var isNavigateToRegisterScreen : Bool = false
    @Published var isNavigateToLoginScreen : Bool = false
}
