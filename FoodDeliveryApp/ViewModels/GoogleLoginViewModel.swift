//
//  GoogleLoginViewModel.swift
//  FoodDeliveryApp
//
//  Created by rafiul hasan on 27/12/21.
//

import Foundation
import Firebase
import GoogleSignIn

final class GoogleSignInViewModel: ObservableObject {

    @Published var isUserLoggedIn = false

    func signIn() {
        do {
            let config = GIDConfiguration.getGIDConfigurationInstance()
            let uiViewController = try UIApplication.getRootViewController()

            GIDSignIn.sharedInstance.signIn(with: config, presenting: uiViewController) { user, error in
                guard error == nil else { return }
                guard user != nil else { return }
                self.isUserLoggedIn = true
          }
        } catch {
            print(error.localizedDescription)
        }
    }

    func signOut() {
        GIDSignIn.sharedInstance.signOut()
        isUserLoggedIn = false
    }
}
